import 'package:apple_store_app/component/component_items.dart';
import 'package:apple_store_app/model/goods_item.dart';
import 'package:apple_store_app/model/goods_item_detail.dart';
import 'package:apple_store_app/pages/page_detail.dart';
import 'package:flutter/material.dart';
import 'package:apple_store_app/component/component_promotion_items.dart';


class PageIndex extends StatefulWidget {
  const PageIndex({super.key});

  @override
  State<PageIndex> createState() => _PageIndexState();
}

  SliverGridDelegate _sliverGridDelegate() {
  return const SliverGridDelegateWithFixedCrossAxisCount(
    crossAxisCount: 2,
    crossAxisSpacing: 2, //수직padding
    mainAxisSpacing: 5, //수평padding
    // childAspectRatio: 1/1,
    );
  }

  class _PageIndexState extends State<PageIndex> {
    List<GoodsItem> _list = [
      GoodsItem(1, 'Mac', 'assets/main/SH_1_Mac.png'),
      GoodsItem(2, 'iPhone', 'assets/main/SH_2_iPhone.png'),
      GoodsItem(3, 'iPad', 'assets/main/SH_3_iPad.png'),
      GoodsItem(4, 'Apple Watch', 'assets/main/SH_4_Apple_Watch.png'),
      GoodsItem(5, 'Apple TV 4k', 'assets/main/SH_5_Apple_TV.png'),
      GoodsItem(6, 'Air Pods', 'assets/main/SH_6_Air_Pods.png'),
    ];

    List<GoodsItemDetail> _listDetail = [
      GoodsItemDetail(1, 'Mac Book Air 15', '더욱 새로워진 Mac Book Air 15!\n새학기 프로모션으로 더욱 저렴하게!', 'assets/main/Promotion_mac_book_air15.jpg'),
      GoodsItemDetail(2, 'iPad Pro 11', '막강한 성능의 M2 칩을 탑재한 iPad Pro 11!', 'assets/main/Promotion_iPad_2.jpg'),
      GoodsItemDetail(3, 'Apple Pencil', 'Mac 또는 iPad 구입 시\nApple Pencil을 받을 수 있습니다!', 'assets/main/Promotion_apple_pencil_3.jpg'),
      GoodsItemDetail(4, 'Watch Series 9','보다 똑똑.보다 또렷.보다 강력!', 'assets/main/SH_4_Apple_Watch.png'),
    ];



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.black,
        elevation: 0,
        title: Text('Apple Store',
          style: TextStyle(
            color: Colors.white,
            fontSize: 20,
            fontWeight: FontWeight.w700,
          ),
            ),
        leading: IconButton(
          icon: Image.asset("assets/apple_logo.png", width: 26, height: 26,),
          onPressed: () {
          },
        ),
        actions: <Widget> [
          IconButton(
            //padding: EdgeInsets.fromLTRB(0, 0, 20, 0),
              onPressed: () {},
              icon: Icon(Icons.shopping_cart,
              color: Colors.white,))
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              width: 500,
              color: Colors.black,
              child: Column(
                children: [
                  Container(
                    child: Column(
//                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          margin: EdgeInsets.fromLTRB(0, 20, 0, 0),
                          child: Text('NEW',
                            style: TextStyle(
                              fontSize: 14,
                              color: Colors.grey[600],
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.fromLTRB(0, 2, 0, 0),
                          child: Text('iPhone 15 Pro',
                            style: TextStyle(
                              fontSize: 35,
                              color: Colors.white,
                              fontWeight: FontWeight.w800,
                            ),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.fromLTRB(0, 3, 0, 0),
                          child: Text('초강력, 초경량. 완전히 새로운 티타늄 디자인, Apple 사상\n가장 앞선 48MP 메인 카메라, 그리고 게임의 판도를 바꾸는\nA17 Pro 지금 자세히 알아보세요.',
                            style: TextStyle(
                              letterSpacing: 0.6,
                              height: 1.6,
                              fontSize: 13,
                              color: Colors.white,
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    child: Image.asset('assets/main/15pro_main.png'),
                    width: 150,
                    margin: EdgeInsets.fromLTRB(0, 5, 0, 0),
                  ),
                ],
              ),
            ),
            Container(
              color: Colors.white,
              child: Column(
                children: [
                  Container(
                    alignment: Alignment.topLeft,
                    margin: EdgeInsets.fromLTRB(35, 40, 0, 10),
                    child: Text(
                      '제품별로 쇼핑하기',
                      style: TextStyle(
                        fontSize: 24,
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                  ),
                  // 상품list
                  GridView.builder(
                    itemCount: _list.length,
                    gridDelegate: _sliverGridDelegate(),
                    itemBuilder: (BuildContext ctx, int idx){
                      return ComponentItems(
                          goodsItem: _list[idx],
                          callback: () {
                            Navigator.of(context).push(MaterialPageRoute(builder: (context) => PageDetail(goodsItem: _list[idx])));
                          },
                      );
                    },
                    shrinkWrap: true,
                    physics: NeverScrollableScrollPhysics(),
                  ),
                ],
              ),
            ),
            Container(
              color: Colors.white,
              child: Column(
                children: [
                  Container(
                    alignment: Alignment.topLeft,
                    margin: EdgeInsets.fromLTRB(35, 80, 0, 20),
                    child: Text('새 학기 프로모션',
                      style: TextStyle(
                        fontSize: 24,
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 20,),
                    child: Container(
                      margin: EdgeInsets.fromLTRB(0, 0, 0, 30),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(topLeft:Radius.circular(20),
                            bottomLeft: Radius.circular(20)),
                        border: Border(
                          left: BorderSide(color: Colors.grey, width: 2),
                          top: BorderSide(color: Colors.grey, width: 2),
                          bottom: BorderSide(color: Colors.grey, width: 2),
                        )
                      ),
                      width: 750,
                      height: 550,
                      child: PageView.builder(
                        controller: PageController(
                          initialPage: 0,
                          viewportFraction: 0.9,
                        ),
                        //scrollDirection: Axis.horizontal,
                        itemBuilder: (BuildContext context, int index){
                          return ComponentPromotionItems(
                              goodsItemDetail: _listDetail[index],
                              callback: () {}
                          );
                        },
                        itemCount: _listDetail.length,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
