// import 'package:apple_store_app/component/component_goods_item.dart';
// import 'package:apple_store_app/model/goods_item.dart';
// import 'package:apple_store_app/pages/page_goods_detail.dart';
// import 'package:flutter/material.dart';
//
// class PageGoodsList extends StatefulWidget {
//   const PageGoodsList({super.key});
//
//   @override
//   State<PageGoodsList> createState() => _PageGoodsListState();
// }

//class _PageGoodsListState extends State<PageGoodsList> {
  // List<GoodsItem> _list = [
  //   GoodsItem(1, 'Mac', 'assets/main/SH_1_Mac.png'),
  //   GoodsItem(2, 'iPhone', 'assets/main/SH_2_iPhone.png'),
  //   GoodsItem(3, 'iPad', 'assets/main/SH_3_iPad.png'),
  //   GoodsItem(4, 'Apple Watch', 'assets/main/SH_4_Apple_Watch.png'),
  //   GoodsItem(5, 'Apple TV 4k', 'assets/main/SH_5_Apple_TV.png'),
  //   GoodsItem(6, 'Air Pods', 'assets/main/SH_6_Air_Pods.png'),
  // ];

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: const Text('상품 리스트'),
//       ),
//       body: ListView.builder(
//         itemCount: _list.length,
//         itemBuilder: (BuildContext ctx, int idx){
//           return ComponentGoodsItem(
//               goodsItem: _list[idx],
//               callback: () {
//                 Navigator.of(context).push(MaterialPageRoute(builder: (context) => PageGoodsDetail(goodsItem: _list[idx])));
//               }
//           );
//         },
//       ),
//     );
//   }
// }
