import 'package:apple_store_app/model/goods_item.dart';
import 'package:flutter/material.dart';

class PageDetail extends StatefulWidget {
  const PageDetail({
    super.key,
    required this.goodsItem
  });

  final GoodsItem goodsItem;

  @override
  State<PageDetail> createState() => _PageDetailState();
}

class _PageDetailState extends State<PageDetail> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
    );
  }
}
