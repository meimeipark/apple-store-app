import 'package:apple_store_app/model/goods_item.dart';
import 'package:flutter/material.dart';

class ComponentItems extends StatelessWidget {
  const ComponentItems({
    super.key,
    required this.goodsItem,
    required this.callback
  });

  final GoodsItem goodsItem;
  final VoidCallback callback;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: callback,
      child: Container(
        child: Column(
          children: [
            Container(
              alignment: Alignment.topLeft,
              margin: EdgeInsets.fromLTRB(60, 20, 0, 5),
              child: Text(goodsItem.goodsTitle,
              style: TextStyle(
                fontWeight: FontWeight.w700,
                fontSize: 16,
                ),
              ),
            ),
            Container(
              width: 150,
              height: 130,
              margin: EdgeInsets.fromLTRB(0, 10, 0, 10),
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage(goodsItem.imgUrl)
                ),
                borderRadius: BorderRadius.circular(20),
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.2),
                    spreadRadius: 0,
                    blurRadius: 9.0,
                    offset: Offset(10, 10),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
