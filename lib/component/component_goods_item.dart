import 'package:apple_store_app/model/goods_item.dart';
import 'package:flutter/material.dart';

class ComponentGoodsItem extends StatelessWidget {
  const ComponentGoodsItem({
    super.key,
    required this.goodsItem,
    required this.callback
  });

  final GoodsItem goodsItem;
  final VoidCallback callback;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: callback,
      child: Container(
        child: Row(
          children: [
            SizedBox(
              child: Text(goodsItem.imgUrl),
            ),
            Column(
              children: [
                Text(goodsItem.goodsTitle),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
