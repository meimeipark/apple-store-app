import 'package:apple_store_app/model/goods_item.dart';
import 'package:apple_store_app/model/goods_item_detail.dart';
import 'package:flutter/material.dart';

class ComponentPromotionItems extends StatelessWidget {
  const ComponentPromotionItems({
    super.key,
    required this.goodsItemDetail,
    required this.callback
  });

  final GoodsItemDetail goodsItemDetail;
  final VoidCallback callback;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: callback,
      child: Container(
        alignment: Alignment.bottomLeft,
        child: Column(
          children: [
            Container(
              alignment: Alignment.bottomLeft,
              margin: EdgeInsets.fromLTRB(20, 60, 0, 0),
              child: Text('NEW SEMESTER',
              style: TextStyle(
                fontSize: 15,
                fontWeight: FontWeight.w600,
                ),
              ),
            ),
            Container(
              alignment: Alignment.bottomLeft,
              width: 600,
              margin: EdgeInsets.fromLTRB(20, 0, 0, 3),
              child: Text(goodsItemDetail.goodsDetailTitle,
              style: TextStyle(
                fontSize: 32,
                fontWeight: FontWeight.w900,
                letterSpacing: 0.7,
                ),
              ),
            ),
            Container(
              alignment: Alignment.bottomLeft,
              width: 600,
              margin: EdgeInsets.fromLTRB(20, 20, 20, 60),
              child: Text(goodsItemDetail.goodsDetailContents,
                style: TextStyle(
                  fontSize: 16,
                  letterSpacing: 0.6,
                  height: 1.6,
                ),
              ),
            ),
            Container(
              alignment: Alignment.center,
              child: Image.asset(
                goodsItemDetail.imgDetailUrl,
                fit: BoxFit.cover,
              ),
              width: 200,
              height: 200,
            ),
          ],
        ),
      ),
    );
  }
}
