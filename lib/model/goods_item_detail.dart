class GoodsItemDetail {
  num id;
  String goodsDetailTitle;
  String goodsDetailContents;
  String imgDetailUrl;

  GoodsItemDetail(this.id, this.goodsDetailTitle, this.goodsDetailContents, this.imgDetailUrl);
}